
// Cau 1:
function kiemTraSoNguyenTo(number){
    var kiemTra = true;
    for(var i=2;i<=Math.sqrt(number);i++){
        if(number%i===0){
            kiemTra = false;
            break;
        }
    }

    return kiemTra;
}

function timSoNguyenTo(){
    event.preventDefault();
    var nhapSo = document.getElementById("txt-nhap-so").value*1;
    var ketQua = "";
    for(var i=2; i<=nhapSo; i++){
        var  kiemTra = kiemTraSoNguyenTo(i);
        if(kiemTra){
            ketQua+= i + " ";
        }
    }
    document.getElementById("result").innerHTML =`Các số nguyên tố là: `+ ketQua;
}